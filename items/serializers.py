from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination

from .models import items
from rest_framework import serializers

class ItemsSerializers(serializers.ModelSerializer):
    class Meta:
        model = items
        # fields = ('itemsID', 'itemsName', 'itemsPrice', 'itemsStartTime', 'itemsEndTime', 'itemsNumber','itemsMaxNum', 'remark',)
        fields = '__all__'
class ItemsPageNumberPagination(PageNumberPagination):
    page_size = 4
    page_query_param =  "page"
    page_size_query_param = "limit"
    max_page_size = 1000000
    class Meta:
        model = items
        fields = '__all__'
