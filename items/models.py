from django.db import models
import uuid
# Create your models here.
from users.models import user


def uuid_general():
    return uuid.uuid1
class items(models.Model):
    objects = models.Manager()
    itemsID = models.UUIDField(primary_key=True, default=uuid_general(), verbose_name='物品编码')
    itemsName = models.CharField(null=False,max_length=50,verbose_name='物品名称')
    itemsNumber = models.IntegerField(null=True,verbose_name='物品剩余数量')
    itemsMaxNum = models.IntegerField(null=False,verbose_name='物品最大数量')
    itemsPrice = models.DecimalField(null=False,max_digits=10,decimal_places=2,verbose_name='物品单价')
    itemsPublisher = models.ForeignKey(user,on_delete=models.CASCADE,verbose_name='物品发布人',null=True)
    itemsStartTime = models.DateField(null = False,verbose_name='物品开放租赁时间')
    itemsEndTime = models.DateField(null = True,verbose_name='物品结束租赁时间')
    remark = models.CharField(null=True,max_length=50,verbose_name='物品备注')
    itemsType = models.CharField(max_length=10,null=False,verbose_name='物品类型',default="待赋值")
    itemsPicture = models.CharField(max_length=100,verbose_name='物品图片',default="0.jpg")


    class Meta:
        db_table = 'items'
        verbose_name = '物品信息表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.itemsID