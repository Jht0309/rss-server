from django.conf.urls import url
from django.urls import path,re_path
from . import views

urlpatterns = {
  # path("", views.logInOut, name = 'logInOut'),
  # re_path(r'(?P<username>\d{11})/',views.msg,name = 'msg')
  path("",views.optItems),
  path("search/",views.MySearchView()),
  url(r'([0-9a-zA-z]{32})/$',views.makeOrder),
  # url('1/',views.msg)
}