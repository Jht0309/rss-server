from django.db import models
import uuid
# Create your models here.
from users.models import user


def uuid_general():
    return uuid.uuid1
class orders(models.Model):
    objects = models.Manager()
    orderID = models.UUIDField(primary_key=True, default=uuid_general(), verbose_name='订单编码')
    buyer = models.ForeignKey(user,on_delete=models.CASCADE,related_name='buyer',verbose_name='订单发起人',null=False)
    seller = models.ForeignKey(user,on_delete=models.CASCADE,related_name='seller',verbose_name='订单承受人',null=False)
    orderStatus = models.CharField(max_length=10,null=False,verbose_name='订单状态')
    orderPrice = models.DecimalField(max_digits=10,decimal_places=2,null=False,verbose_name='订单价格')
    orderDate = models.DateTimeField(null=False,verbose_name='订单生成时间')
    orderItemsName = models.CharField(null=False,max_length=50,verbose_name='物品名称')
    orderItemsID = models.CharField(null=False,max_length=40,verbose_name='物品ID')
    orderItemsNum = models.IntegerField(default = 0, verbose_name='物品数量')
    orderItemsPrice = models.DecimalField(max_digits=10,decimal_places=2,null=False,verbose_name='物品单价')
    orderItemsPicture = models.CharField(max_length=100,verbose_name='订单显示物品图片',default="?")
    class Meta:
        db_table = 'orders'
        verbose_name = '订单信息表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.orderID