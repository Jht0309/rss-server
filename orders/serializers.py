from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination

from .models import orders
from rest_framework import serializers

class OrdersSerializers(serializers.ModelSerializer):
    class Meta:
        model = orders
        # fields = ('itemsID', 'itemsName', 'itemsPrice', 'itemsStartTime', 'itemsEndTime', 'itemsNumber','itemsMaxNum', 'remark',)
        fields = '__all__'
class OrdersPageNumberPagination(PageNumberPagination):
    page_size = 100000
    page_query_param =  "page"
    page_size_query_param = "limit"
    max_page_size = 1000000
    class Meta:
        model = orders
        fields = '__all__'
