from django.conf.urls import url
from django.urls import path,re_path
from . import views

urlpatterns = {
  # path("", views.logInOut, name = 'logInOut'),
  # re_path(r'(?P<username>\d{11})/',views.msg,name = 'msg')
  path('',views.logInOut),
  path('0/',views.selfmsg),
  url(r'([0-9]{11})/$',views.msg),
  url(r'0/items/$',views.res),
  url(r'0/items/([0-9a-zA-z]{32})/$',views.optItems),
  path(r'0/orders/',views.ord),
  url(r'0/orders/([0-9a-zA-z]{32})/$',views.optOrders),
  # url('1/',views.msg)
}