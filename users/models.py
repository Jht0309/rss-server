import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.
def uuid_general():
    return uuid.uuid1
class user(AbstractUser):
    stuID = models.CharField(max_length=11,unique=True,verbose_name='学号')
    nickname = models.CharField(max_length=20,unique=True,verbose_name='昵称')
    userPro = models.CharField(max_length=40,verbose_name='密保问题')
    userAns = models.CharField(max_length=100,verbose_name='密保答案')
    uuid = models.UUIDField(primary_key=True,default=uuid_general(),verbose_name='用户编码')
    userPicture = models.CharField(max_length=100,verbose_name='用户头像',default="?")
    # userPicture = models.CharField(max_length=50000,verbose_name='用户头像')
    class Meta:
        db_table = 'user'
        verbose_name = '用户信息'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username




